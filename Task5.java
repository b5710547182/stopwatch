import java.math.BigDecimal;

/**
 * Use this class to sum given  Big decimal values from an array.
 * @author Thanawit Gerdprasert
 *
 */
public class Task5 implements Runnable{

	private int counter;
	static final int ARRAY_SIZE = 500000;
	public BigDecimal[] values;
	BigDecimal sum;


	/**
	 * Create the task with the initialize of the counter and the array of BigDecimal.
	 * @param counter is the number of character that will be calculated.
	 */
	public Task5(int counter)
	{
		this.counter = counter;
		values = new BigDecimal[ARRAY_SIZE];
	}

	/**
	 * Run the program and then sum the BigDecimal object that has same value as task3.
	 */
	public void run() {

		for(int i=0; i<ARRAY_SIZE; i++) values[i] = new BigDecimal(i+1);
		BigDecimal sum = new BigDecimal(0.0);
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum.add( values[i] );
		}


	}
	/**
	 * Show the result after run.
	 * @return return the String that show the length of word that appended
	 */
	public String toString()
	{
		return ("sum = " + sum);
	}

}
