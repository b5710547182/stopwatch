/**
 * Use this class to sum given  double Object values from an array.
 * @author Thanawit Gerdprasert
 *
 */
public class Task4 implements Runnable{
	static final int ARRAY_SIZE = 500000;
	private int counter;
	private double sum;
	
	/**
	 * Create the task with the initialize of the counter and the String.
	 * @param counter is the number of character that will be calculated.
	 */
	public Task4(int counter)
	{
		this.counter = counter;
		this.sum=0.0;
	}
	
	/**
	 * Run the program and then sum the double object that has same value as task3.
	 */
	public void run() {
		System.out.printf("Sum array of Double objects with count=%,d\n", counter);
		Double[] values = new Double[ARRAY_SIZE];
		for(int i=0; i<ARRAY_SIZE; i++) values[i] = new Double(i+1);
		sum = new Double(0.0);
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}


	}
	
	/**
	 * Show the result after run.
	 * @return return the String that show the length of word that appended
	 */
	public String toString()
	{
		return "sum = " + sum;
	}

	
}
