/**
 * Use this class to sum given  double values from an array.
 * @author Thanawit Gerdprasert
 *
 */
public class Task3 implements Runnable {
	static final int ARRAY_SIZE = 500000;
	private int counter;
	private double sum = 0.0;
	
	/**
	 * Create the task with the initialize of the counter and the String.
	 * @param counter is the number of character that will be calculated.
	 */
	public Task3(int counter)
	{
		this.counter = counter;
		this.sum = 0;
	}
	
	
	/**
	 * Run the program and sum the double value from an array.
	 */
	public void run() {
		System.out.printf("Sum array of double primitives with count=%,d\n", counter);
		double[] values = new double[ARRAY_SIZE];
		for(int k=0; k<ARRAY_SIZE; k++) values[k] = k+1;
		sum = 0.0;
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
		
	}
	
	/**
	 * Show the result after run.
	 * @return return the String that show the length of word that appended
	 */
	public String toString()
	{
		return "sum = " + sum;
	}

}
