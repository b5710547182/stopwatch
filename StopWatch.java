/**
 * Use this class to get the difference of time between two point.
 * @author Thanawit Gerdprasert
 * @version 26/1/2015
 */
public class StopWatch {
	
	
	private int runningChecker;
	private double currentTime;
	private double endingTime;
	// if runningChecker =1 then it start, else it's not
	
	
	/**
	 * Create a StopWatch object and initialize all the variable to zero.
	 */
	public StopWatch()
	{
		this.currentTime = 0;
		this.endingTime = 0;
		this.runningChecker =0;
	}
	
	
	/**
	 * Start the StopWatch by setting the time to current time if the StopWatch is not running.
	 */
	public void start()
	{
		if(this.isRunning())
		{
			//do nothing
		}
		else
		{
			this.currentTime = System.nanoTime()* 1.0E-9;
			this.runningChecker++;
		}
	}
	
	/**
	 * Stop the StopWatch by getting the ending time if StopWatch is running.
	 */
	public void stop()
	{
		if(this.isRunning())
		{
			this.endingTime =System.nanoTime()* 1.0E-9;
			this.runningChecker--;
		}
		else
		{
			//do nothing
		}
		
	}
	/**
	 * Check whether or not StopWatch is running.
	 * @return true if StopWatch is running, false if StopWatch is not running.
	 */
	public boolean isRunning()
	{
		return this.runningChecker !=0;
		
	}
	/**
	 * Get the time between the Start and Stop if the StopWatch is running, else get difference between
	 * 		the current time and the start time.
	 * @return the time between two point.
	 */ 
	public double getElapsed()
	{
		if(this.isRunning())
		{
			double tempTime = System.nanoTime()* 1.0E-9;
			return tempTime-this.currentTime;
		}
		else
		{
			return this.endingTime - this.currentTime;
		}
	}
	
}
