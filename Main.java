/**
 * Main Class that will create all task and run it and get all the time in different task.
 *@author Thanawit Gerdprasert
 *@version 28/1/2015
 * 
 */
public class Main {

	/**
	 * Run the program after compile and create all 5 tasks followed by running each task with the StopWatch to get the elapse time.
	 * @param args 
	 */
	public static void main(String[] args) {
		Task1 task1 = new Task1(100000);
		Task2 task2 = new Task2(100000);
		Task3 task3 = new Task3(100000000);
		Task4 task4 = new Task4(100000000);
		Task5 task5 = new Task5(100000000);
		
		System.out.println("Task 1 append 100,000 character to String");
		TaskTimer.measureAndPrint(task1);
		System.out.println("Task 2 append 100,000 character to String by using String Builder");
		TaskTimer.measureAndPrint(task2);
		System.out.println("Task 3 sum 100,000,000 double from array");
		TaskTimer.measureAndPrint(task3);
		System.out.println("Task 4 sum 100,000,000 Double objects");
		TaskTimer.measureAndPrint(task4);
		System.out.println("Task 5 sum 100,000,000 BigDecimal objects ");
		TaskTimer.measureAndPrint(task5);
	}

}
