/**
 * Use this class to create StopWatch and Run the program.
 * @author Thanawit Gerdprasert
 *
 */
public class TaskTimer {


	/**
	 * Called by Main to make it run with the object that it will be used with.
	 * @param runnable from the interface that will be the data that will run in the program
	 */
	public static void measureAndPrint(Runnable runnable)
	{
		StopWatch runner = new StopWatch();
		runner.start();
		runnable.run();
		runner.stop();
		System.out.println("Total Time is "+runner.getElapsed());	
	}
	
}
