/**
 * Use this class to append given characters to a StringBuilder, then convert result to String and display its length.
 * @author Thanawit Gerdprasert
 *
 */
public class Task2 implements Runnable {

	private String result;
	private int counter;
	
	/**
	 * Create the task with the initialize of the counter and the String result.
	 * @param counter is the number of character that will be appended.
	 */
	public Task2(int counter)
	{
		this.counter = counter;
		this.result = "";
	}
	/**
	 * Run the program and then add char to String builder depend on number of counter.
	 */
	public void run() {
		final char test = 'a';
		System.out.printf("Append to StringBuilder with count=%,d\n", counter);
		StringBuilder builder = new StringBuilder(); 
		int k = 0;
		while(k++ < counter) {
			builder = builder.append(test);
		}
		// now create a String from the result, to be compatible with task 1.
		result = builder.toString();
		
		
	}
	/**
	 * Show the result after run.
	 * @return return the String that show the length of word that appended
	 */
	public String toString()
	{
		return "final string length = " + result.length();
	}

}
