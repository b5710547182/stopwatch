

/**
 * Use this class to append given characters to a String. Display length of the result.
 * @author Thanawit Gerdprasert
 *
 */
public class Task1 implements Runnable{

	private int counter = 0;
	private String sum = "";
	
	/**
	 * Create the task with the initialize of the counter and the String sum.
	 * @param counter is the number of character that will be appended.
	 */
	public Task1(int counter)
	{
		this.counter = counter;
		this.sum = "";
	}
	
	/**
	 * Run the program and then add the String depend on number of counter.
	 */
	public void run() {
		final char text = 'a';
		System.out.printf("Append to String with count=%,d\n", counter);
		sum = ""; 
		int k = 0;
		while(k++ < counter) {
			sum = sum + text;
		}
		
	}
	/**
	 * Show the result after run.
	 * @return return the String that show the length of word that appended
	 */
	public String toString()
	{
		return String.format("final string length = " + sum.length());
	}

	
	
}

