 Stopwatch by Thanawit Gerdprasert
I ran the tasks on a Acer Laptop Aspire E15, and got these
results:
Task | Time
--------------------------------------------------------------------------------|---------------------------:
Append 100,000 chars to String 			          			| 5.773971086979145 sec
Append 100,000 characters to a StringBuilder then convert result to String 	| 0.0031143629748839885 sec
Sum 100,000,000 double values from an array					| 0.2768409060081467 sec
Sum 100,000,000 Double objects having the same values as in task 3.		| 0.3684435059840325 sec
Sum 100,000,000 BigDecimal objects having the same values as in task 3.		| 2.083363529993221
--------------------------------------------------------------------------------|---------------------------:

Compare the first one and the second one : The reason first one is longer probably because of String Builder is keeping 
all the file as Object and then when it finish it transform itself to String not like the first one which create String
and continue to add more and more stuff to it.

Compare the third , four and, five task : double as the primitive data type take the least time because it is in the 
programs and doesn't need to create any object followed by the Object of Double that will need to be create each time 
you called the code and the last is BigDecimal which needed to be import from Java library first and then we can use it
after making it the slowest of all 3. 
